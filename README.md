### Commands for running bash script remotley
```bash
 # connecting to ubuntu
 > ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.254.239.71

 
 # command to send script file

 scp -i <secure_key> <file> <user>@<IP_address>:<path_directory>

 > scp -i ~/.ssh/ch9_shared.pem bash_script.sh ubuntu@3.249.165.210:/home/ubuntu/arslan.sh

# command to send html for website

 scp -i <secure_key> -r <folder> <user>@<IP_address>:<file_directory>

 > scp -i ~/.ssh/ch9_shared.pem -r arslan_website ubuntu@3.249.165.210:/home/ubuntu/

 # command to run bash script

 ssh -i <secure_key> <user>@<IP_address>: bash <bash_file_path>

 > ssh -i ~/.ssh/ch9_shared.pem ubuntu@3.249.165.210 bash /home/ubuntu/arslan.sh

 # moving files on ubuntu

 > sudo mv ~/arslan_website/index.html /var/www/html/index.html 



```